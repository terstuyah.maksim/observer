  class Publisher {
    let instance = null;
    #pubHolder = null;
    constructor() {
      if (instance) {
        return instance;
      }
      this.#pubHolder = {};
      instance = this;
    }
    on(type, cb) {
      if (!this.#pubHolder[type]) {
        this.#pubHolder[type] = [];
      }
      this.#pubHolder[type].push(cb);
    }
    off(type, cb) {
      if (!this.#pubHolder[type]) {
        return;
      }
      this.#pubHolder[type] = this.#pubHolder[type].filter(cb => cb !== cb);
    }
    fire(type, payload) {
      if (!this.#pubHolder[type]) {
        return;
      }
      this.#pubHolder[type].forEach(el => el(payload));
    }
  }